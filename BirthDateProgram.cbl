       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BirthDateProgram.
       AUTHOR. Apantri Sohheng.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 BirthDate.
           02 YearOfBirth.
              03 Century0B PIC 99.
              03 Year0B    PIC 99.
           02 MonthOfBirth PIC 99.
           02 DayOfBirth   PIC 99.
       PROCEDURE DIVISION .
       BirthDateResult.
           MOVE 19891015 TO BirthDate
           DISPLAY "Month is = " MonthOfBirth 
           DISPLAY "Century of birth is " Century0B 
           DISPLAY "Year of birth is = " YearOfBirth 
           DISPLAY DayOfBirth "/" MonthOfBirth "/" YearOfBirth
           MOVE ZEROES TO  YearOfBirth 
           DISPLAY "Birth date= " BirthDate 
           STOP RUN .